<?php

namespace Drupal\typed_link\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'typed_link' field widget.
 */
#[FieldWidget(
  id: 'typed_link',
  label: new TranslatableMarkup('Typed Link Widget'),
  description: new TranslatableMarkup('Expands on the link widget adding a drop down for the asset type.'),
  field_types: ['typed_link'],
)]
class TypedLinkWidget extends LinkWidget {

  /**
   * Widget options.
   *
   * @var array
   */
  protected array $options;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Constructs a new Typed link field widget.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   The widget third party settings.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, AccountProxyInterface $currentUser, ModuleHandler $moduleHandler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->currentUser = $currentUser;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['link_type'] = [
      '#title' => $this->t('Link type'),
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => $this->getSelectedOption($items[$delta]),
      '#empty_option' => $this->t('- Select -'),
      '#multiple' => FALSE,
      '#required' => $element['#required'],
    ];

    // Make the type required on the front-end when URI filled-in.
    $parents = $element['#field_parents'];
    $parents[] = $this->fieldDefinition->getName();
    $selector = $root = array_shift($parents);
    if ($parents) {
      $selector = $root . '[' . implode('][', $parents) . ']';
    }

    $element['link_type']['#states']['required'] = [
      ':input[name="' . $selector . '[' . $delta . '][uri]"]' => ['filled' => TRUE],
    ];

    return $element;
  }

  /**
   * Get the column name of the field.
   *
   * @return string
   *   The column name.
   */
  protected function getColumn(): string {
    return $this->fieldDefinition->getFieldStorageDefinition()->getMainPropertyName();
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity): array {
    if (!isset($this->options)) {
      // Limit the settable options for the current user account.
      $options = $this->fieldDefinition
        ->getFieldStorageDefinition()
        ->getOptionsProvider($this->getColumn(), $entity)
        ->getSettableOptions($this->currentUser);

      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
      ];
      $this->moduleHandler->alter('options_list', $options, $context);

      array_walk_recursive($options, [$this, 'sanitizeLabel']);

      $options = OptGroup::flattenOptions($options);

      $this->options = $options;
    }
    return $this->options;
  }

  /**
   * Determines selected options from the incoming field values.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field values.
   *
   * @return string|null
   *   The selected option or null if there is none.
   */
  protected function getSelectedOption(FieldItemInterface $item): ?string {
    // We need to check against a flat list of options.
    $flat_options = OptGroup::flattenOptions($this->getOptions($item->getEntity()));

    $value = $item->{$this->getColumn()};
    $selected_option = NULL;
    // Keep the value if it actually is in the list of options (needs to be
    // checked against the flat list).
    if (isset($flat_options[$value])) {
      $selected_option = $value;
    }

    return $selected_option;
  }

  /**
   * Sanitize label.
   *
   * @param string $label
   *   The label.
   */
  protected function sanitizeLabel(string &$label): void {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

}
