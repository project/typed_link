# Use Case
The original use case for needed this module was a website needed to store a lot of files on a separate CDN with no API access.

In order to make these files available a link field could be easily used but we wanted to be able to add icons to the links. This is where the drop down category comes in.

# How it works

This field extends the link field, widget and formatter. It uses the text list field for applying a link type.

