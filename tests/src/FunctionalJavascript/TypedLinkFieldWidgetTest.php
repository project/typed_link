<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_link\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests the Typed link field widget.
 */
class TypedLinkFieldWidgetTest extends WebDriverTestBase {

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'user',
    'text',
    'link',
    'options',
    'typed_link',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Page']);

    FieldStorageConfig::create([
      'field_name' => 'typed_link_field',
      'entity_type' => 'node',
      'type' => 'typed_link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'allowed_values' => [
          'type1' => 'Type 1',
          'type2' => 'Type 2',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'typed_link_field',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => 'Typed link',
      'required' => TRUE,
      'settings' => [],
    ])->save();

    $form_display = $this->container->get('entity_type.manager')
      ->getStorage('entity_form_display')
      ->load('node.page.default');

    $form_display->setComponent('typed_link_field', [
      'weight' => 1,
      'region' => 'content',
      'type' => 'typed_link',
      'settings' => [
        'placeholder_url' => '',
        'placeholder_title' => '',
      ],
      'third_party_settings' => [],
    ])->save();
  }

  /**
   * Tests the Typed link field widget.
   */
  public function testTypedLinkFieldWidget(): void {
    $user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($user);

    $this->drupalGet('/node/add/page');

    $this->getSession()->getPage()->fillField('title[0][value]', 'My page');
    $this->getSession()->getPage()->fillField('URL', 'http://example.com');

    // Disable the browser required field validation to have form validation.
    $this->getSession()->evaluateScript("typeof jQuery === 'undefined' || jQuery(':input[required]').prop('required', false);");
    $this->getSession()->getPage()->pressButton('Save');

    // We require selecting a link type by Drupal states.
    $this->assertSession()->pageTextContainsOnce('Link type field is required.');

    $this->assertFieldSelectOptions('Link type', [
      'type1',
      'type2',
    ]);
    $this->getSession()->getPage()->selectFieldOption('Link type', 'Type 1');
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextContainsOnce('Page My page has been created.');

    $node = $this->getNodeByTitle('My page');
    $this->drupalGet($node->toUrl('edit-form'));

    $this->assertSession()->fieldValueEquals('URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Link text', '');
    $this->assertTrue($this->assertSession()->optionExists('Link type', 'Type 1')->isSelected());

    $this->getSession()->getPage()->fillField('Link text', 'Example link');
    $this->getSession()->getPage()->pressButton('Save');
    $this->assertSession()->pageTextContainsOnce('Page My page has been updated.');
    $this->drupalGet($node->toUrl('edit-form'));

    $this->assertSession()->fieldValueEquals('URL', 'http://example.com');
    $this->assertSession()->fieldValueEquals('Link text', 'Example link');
    $this->assertTrue($this->assertSession()->optionExists('Link type', 'Type 1')->isSelected());
  }

  /**
   * Checks if a select element contains the specified options.
   *
   * @param string $name
   *   The field name.
   * @param array $expected_options
   *   An array of expected options.
   */
  protected function assertFieldSelectOptions(string $name, array $expected_options): void {
    $select = $this->getSession()->getPage()->find('named', [
      'select',
      $name,
    ]);

    if (!$select) {
      $this->fail('Unable to find select ' . $name);
    }

    $options = $select->findAll('css', 'option');
    array_walk($options, function (NodeElement &$option) {
      $option = $option->getValue();
    });
    $options = array_filter($options);
    sort($options);
    sort($expected_options);
    $this->assertSame($expected_options, $options);
  }

}
