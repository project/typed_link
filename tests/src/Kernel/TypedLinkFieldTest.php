<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_link\Kernel;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the Typed link field type and formatter.
 */
class TypedLinkFieldTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'node',
    'field',
    'user',
    'text',
    'options',
    'typed_link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig([
      'system',
      'node',
      'field',
      'user',
    ]);

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    $this->container->get('entity_type.manager')->getStorage('node_type')->create([
      'type' => 'page',
      'name' => 'Page',
    ])->save();

    FieldStorageConfig::create([
      'field_name' => 'typed_link',
      'entity_type' => 'node',
      'type' => 'typed_link',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'allowed_values' => [
          'type1' => 'Type 1',
          'type2' => 'Type 2',
        ],
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'typed_link',
      'bundle' => 'page',
    ])->save();
  }

  /**
   * Tests the field type and formatter.
   */
  public function testTypedLinkField(): void {
    /** @var \Drupal\node\NodeStorageInterface $node_storage */
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');

    $values = [
      [
        'title' => 'Front page',
        'uri' => 'http://drupal.org',
        'link_type' => 'type1',
        'options' => [],
      ],
      [
        'title' => 'Example link',
        'uri' => 'http://example.com',
        'link_type' => 'type2',
        'options' => [],
      ],
    ];

    /** @var \Drupal\node\NodeInterface $node */
    $node = $node_storage->create([
      'type' => 'page',
      'title' => 'Test page',
      'typed_link' => $values,
    ]);
    // Assert values are saved in the field.
    $this->assertEquals($values, $node->get('typed_link')->getValue());

    // Assert formatter rendering.
    $builder = $this->container->get('entity_type.manager')->getViewBuilder('node');
    $build = $builder->viewField($node->get('typed_link'));
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->assertStringContainsString('<div>typed_link</div>', (string) $output);
    $this->assertStringContainsString('<div><a href="http://drupal.org">Front page</a>Type 1</div>', (string) $output);
    $this->assertStringContainsString('<div><a href="http://example.com">Example link</a>Type 2</div>', (string) $output);
  }

}
